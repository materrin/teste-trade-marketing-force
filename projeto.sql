
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `projeto`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--

DROP TABLE IF EXISTS `empresa`;
CREATE TABLE IF NOT EXISTS `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE utf8_bin NOT NULL,
  `endereco` varchar(60) COLLATE utf8_bin NOT NULL,
  `telefone` int(10) NOT NULL,
  `cidade` varchar(60) COLLATE utf8_bin NOT NULL,
  `uf` varchar(2) COLLATE utf8_bin NOT NULL,
  `cep` int(8) NOT NULL,
  `categoria` varchar(60) COLLATE utf8_bin NOT NULL,
  `descricao` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `empresa`
--

INSERT INTO `empresa` (`id`, `nome`, `endereco`, `telefone`, `cidade`, `uf`, `cep`, `categoria`, `descricao`) VALUES
(1, 'Acquamondo', 'Avenida Cruzeiro do Sul, 550', 1454698770, 'Bauru', 'SP', 17089600, 'Saúde, Esporte', 'Academia de pequeno forte, oferece aulas de natação, hidroginástica e ginástica funcional. Piscina semi-olímpica (25 m) com apenas 4 raias. Salão da academia comporta 5 pessoas simultaneamente fazendo aula no mesmo horário.'),
(2, 'Maionese Viagens', 'Alameda das Rosas, 605', 1463328970, 'Lins', 'SP', 16058697, 'Viagem', 'Agência de turismo de pequeno porte, com foco em viagens de formatura.'),
(3, 'Buffet do Vale', 'Rua Antonio Fischer dos Santos, 120', 1630668796, 'São Carlos', 'SP', 13560698, 'Eventos', 'Buffet com salão médio, para eventos como casamentos para no máximo 100 convidados.'),
(4, 'Restaurante Bom de Prato', 'Avenida das Palmeiras, 500', 1532710348, 'Itapetininga', 'SP', 18200520, 'Alimentação', 'Restaurante em vias de expansão com modelo de franquia, self-service (comida por quilo). Funcionamento apenas no horário do almoço.'),
(5, 'Aspen Chocolates', 'Rua dos Padres, 975', 1589647520, 'Sorocaba', 'SP', 169875201, 'Alimentação', 'Fábrica de chocolate de pequeno porte, com loja própria. Iniciando atividades de venda de seus produtos em restaurantes/cafés na região e fora.'),
(6, 'Personal Fit Marmitaria', 'Avenida Duque de Caxias, 243', 1489657032, 'Bauru', 'SP', 17569320, 'Alimentação, Saúde', 'Marmitaria com serviço de montagem e entrega das refeições. Cardápio focado em pessoas com restrições alimentares, seja por alergias/intolerância ou por estilo de vida.'),
(7, 'Clinica Odontológica Sorridente', 'Avenida do Café, 374', 1696325780, 'Ribeirão Bonito', 'SP', 14965802, 'Saúde', 'Clínica odontológica de preço acessível aos pacientes de baixa renda. Realizam-se procedimentos de todas as especialidades.'),
(8, 'Excursões Arroz com farofa', 'Rua Antonio Fischer dos Santos, 590', 126987501, 'São José dos Campos', 'SP', 11580630, 'Eventos, Viagem', 'Companhia especializada em excursões para shows e outros eventos de grande porte fora da cidade, com ida e volta no mesmo dia.'),
(9, 'Global Esporte', 'Travessa dos Cravos, 963', 1896325687, 'Ilha Solteira', 'SP', 19865413, 'Esporte', 'Pequena loja de artigos esportivos.'),
(10, 'Clinicão Veterinários', 'Rua Luis dos Santos, 541', 175648952, 'Potirendaba', 'SP', 15489632, 'Saúde', 'Clínica veterinária para animais de pequeno e médio porte. Conta com serviço de pet-shop.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `login` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `senha` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `login`, `senha`) VALUES
(0000000001, 'admin', 'admin');

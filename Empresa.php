<?php

    class Empresa extends CI_Controller{

        public function index(){
            $this->load->model("Empresa_Model");

            $contatos = $this->Empresa_Model->buscarTodos();
            
            $pacote = array(
                "empresa"=>$empresa,
                "pagina" =>"table.php"
            );
        
            $this->load->view("index",$pacote);

            }
        
        public function novo(){
            $this->load->view('empresaNovo');
        }
        public function salvarNovo(){
            
            $dados = array(
                'nome' => $_POST['nome'],
                'endereco' => $_POST['endereco'],
                'telefone' => $_POST['telefone'],
                'cidade' => $_POST['cidade'],
                'uf' => $_POST['uf'],
                'cep' => $_POST['cep'],
                'categoria' => $_POST['categoria'],
                'descricao' => $_POST['descricao'],
                
            
            );
            $this->load->model("Empresa_Model");
            $this->Empresa_Model->salvarNovo($dados);
            
            redirect('/empresa');

        
        
    }
}
?>